import numpy as np
import sys

# ----------------------------------------- #
# Parametros e Estruturas de dados da MLP   #
# ----------------------------------------- #
nEntradas           = 2
nSaidas             = 1
nCamadasOcultas     = 1
nNeuroniosCamOculta = 2

txAprendizado       = 0.7
nEpocasTreinamento  = 1000

wEntradaOculta      = np.array(np.random.randn(nEntradas, nNeuroniosCamOculta))
wOcultaSaida        = np.array(np.random.randn(nNeuroniosCamOculta)) 
wBiasEntradaOculta  = np.array(np.random.randn(nNeuroniosCamOculta)) 
wBiasOcultaSaida    = np.array(np.random.randn(nSaidas))

vSaidaCamadaOculta  = np.array(np.random.randn(nNeuroniosCamOculta))
vSaida              = 0.0

vGradienteOculta    = np.array(np.random.randn(nNeuroniosCamOculta))

vDeltaOculta        = np.array(np.random.randn(nNeuroniosCamOculta))
vDeltaSaida         = 0.0


# Variável para entrada de dados (X1 e X2)
vInputs             = np.array([[0], [0]])


# ----------------------------------------- #
# Set de treinamento                        #
# ----------------------------------------- #
dadosEntradas  = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
dadosSaidasD   = np.array([[0], [1], [1], [0]]) 


# ----------------------------------------- #
# Rotinas da MLP                            #
# ----------------------------------------- #
def f_sigmoid(net):
    return 1 / (1 + np.exp(-net))


def f_sigmoiDeriv(val):
    return val * (1 - val)


def f_erro(desejado, saida):
    return desejado - saida

    
def f_calcularSaida(vEntradas):
    global nNeuroniosCamOculta, vSaidaCamadaOculta, wEntradaOculta, wBiasEntradaOculta, nEntradas
    global vSaida, wOcultaSaida, wBiasOcultaSaida
    
    # Calcular a saida para cada um dos neurônios da camada oculta
    for j in range(nNeuroniosCamOculta):
        for i in range(nEntradas):
            # Calcular saída da camada oculta
            vSaidaCamadaOculta[j]  = vEntradas[i] * wEntradaOculta[i][j]    
            # Somar com o valor de bias
            vSaidaCamadaOculta[j]  += wBiasEntradaOculta[i]
        # Calcular saída sigmoid
        vSaidaCamadaOculta[j]  = f_sigmoid(vSaidaCamadaOculta[j])
        
    # Calcular saida da camada de saida
    for i in range(nNeuroniosCamOculta):
        vSaida +=  vSaida + vSaidaCamadaOculta[i] * wOcultaSaida[i] 
        
    # Somar com o valor de bias
    vSaida              += wBiasOcultaSaida
    # Calcular saida sigmoid
    vSaida              = f_sigmoid(vSaida)
    
    
def f_backPropagation(desejado, vEntradas):
    global vDeltaSaida, vGradienteOculta, wOcultaSaida, vDeltaOculta, vGradienteOculta
    global vSaidaCamadaOculta, nNeuroniosCamOculta, wOcultaSaida, txAprendizado
    global wBiasOcultaSaida, nNeuroniosCamOculta, nEntradas, vDeltaOculta
    global wBiasEntradaOculta
    
    # Calcular a derivada do erro e o gradiente e corrigir os pesos sinápticos
    
    # Calcular o erro da MLP
    erro                = f_erro(desejado, vSaida)
    # Calcular o delta do erro
    vDeltaSaida         = erro * f_sigmoiDeriv(vSaida)
    
    for i in range(nNeuroniosCamOculta):
        # Calcular o gradiente da camada oculta
        vGradienteOculta[i]    = vDeltaSaida * wOcultaSaida[i] 
        # Calcular o detal da camada oculta
        vDeltaOculta[i]  = vGradienteOculta[i] * f_sigmoiDeriv(vSaidaCamadaOculta[i])

    # Corrigir os pesos sinápticos entre a camada oculta e a camada de saída
    for i in range(nNeuroniosCamOculta):    
        wOcultaSaida[i]  += vSaidaCamadaOculta[i] * vDeltaSaida * txAprendizado
    
    wBiasOcultaSaida  += vDeltaSaida * txAprendizado 
    
    # Corrigir os pesos sinápticos entre a camada de entrada e a camada oculta
    for j in range(nNeuroniosCamOculta):
        for i in range(nEntradas):
            wEntradaOculta[i][j] += vEntradas[i] * vDeltaOculta[i] * txAprendizado
        wBiasEntradaOculta[j]  += vDeltaOculta[i] * txAprendizado

    
def f_treinarMLP():
    global dadosEntradas, dadosSaidasD, nEpocasTreinamento
    
    print("Iniciando treinamento ...")
    for i in range(nEpocasTreinamento):
        print("Epoca: ", i)
        # Para cada item (linha) do conjunto de treinamento
        for t in (range(4)):
            f_calcularSaida(dadosEntradas[t])
            f_backPropagation(dadosSaidasD[t], dadosEntradas[t])
    
    # Mostrar pesos sinápticos após o treinamento    
    print("Fim de treinamento ...\n")
    f_imprimirSinapes()
    

def f_imprimirSinapes():
    global wEntradaOculta, wBiasEntradaOculta, wBiasOcultaSaida, wBiasOcultaSaida
    
    print("Sinapes entrada->camada oculta")
    print(wEntradaOculta)
    print("Sinapse bias -> camada oculta")
    print(wBiasEntradaOculta)
    print("Sinapes camada oculta->saida")
    print(wBiasOcultaSaida)
    print("Sinapse bias -> camada saida")
    print(wBiasOcultaSaida)

def menu():
    
    while True:
        print("Rede Neural Artificial - MLP")
        print("----------------------------")
        print("Problema do OU exclusivo")
        print("----------------------------")
        print("1.Treinar a rede")
        print("2.Mostrar pesos sinapticos")
        print("3.Usar a rede")
        print("4.Sair do programa")
        opcao = int(input("Opcao? "))
        
        if opcao == 1:
            f_treinarMLP()
            
        elif opcao == 2:
            f_imprimirSinapes()
            
        elif opcao == 3:
            vInputs[0] = int(input("Valor 1: "))
            vInputs[1] = int(input("Valor 2: "))
            f_calcularSaida(vInputs)
            print("Saida da MLP: ", vSaida)
            
        elif opcao == 4:
            sys.exit()

# ----------------------------------------- #
# Rotina principal                          #
# ----------------------------------------- #
def main():
    menu()
    
if __name__ == "__main__":
    main()
   
